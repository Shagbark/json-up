package org.level.up.json.test;

import org.level.up.json.SerializedBy;

@SerializedBy(
        serializer = CatJsonSerializer.class,
        deserializer = CatJsonDeserializer.class
)
public class Cat {

    private String name;
    private int age;

    public Cat() {}

    public Cat(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }
}