package org.level.up.json;

public interface JsonDeserializer<T> {
    // https://bitbucket.org/Shagbark/json-up/downloads/
    T deserialize(String json);

}
